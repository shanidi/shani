<?php

namespace app\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
 use yii\filters\AccessControl;
/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller // יורשים מהמחלקה הבסיסי קונטרולר
{
    /**
     * @inheritdoc
     */
    
public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),//כנראה שצריך יוז
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update','delete'],//החוקים חלים רק על הפונקציה עדכון שבקוד
                'rules' => [
                    [
                        'allow' => true,//לאפשר לעשות אפדייט
                        'actions' => ['update','delete'],//על איזו פונקציה מלמעלה מדברים
                        'roles' => ['updateArticle'], // שם ההרשאה במסד הנתונים!!!
                        'roleParams' => function() { // כאשר יש תנאי נוסף פרט להרשה נעביר לחוק פרמטר. כמו למשל שכל אחד יוכל לעדכן את הארטיקל שלו בלבד.
                            return ['article' => Article::findOne(['id' => Yii::$app->request->get('id')])];//מעביר לחוק מידע באיזה ארטיקל מדובר
                        },
                    ],
                    
                ],
            ],
        ];            
    }


    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex() //מביא אותך לאינדקס אם לא תרשום כלום אחרי הארטיקל ביו-אר-אל
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);//כל הפרמטרים שנמצאים ביו אר אל- queryParams- נשלח את הפרמטרים 
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) //מציג רשומה בודדת
    {
     $model=$this->findModel($id);//נשלוף את המאמר כאובייקט לפי איי די
    $tagModels = $model->tags;//מביא מתוך האובייקט את התכונה טאג ומכניס למשתנה טאגמודל
    //שולחים לוויו את הקישורים כקישורים , מחלצים אותם פה
    $tags='';// משתנה ריק מסוג סטרינד
    foreach($tagModels as $tag){
     $tagLink = Html::a($tag->name,['article/index','ArticleSearch[tag]'=> $tag->name]); 
     // ,שימוש בפונקציה שבונה לינק- בסוגריים מה נקבל, [לאן נלך, פרמטר שצריך לקרוא לו-טאג לא פרמטר שמור, חץ, 
    $tags.=','.$tagLink; // שירשור הלינקים
   
    }
        $tags=substr($tags,1); //מוחק את הפסיק הראשון

        return $this->render('view', [
            'model' => $this->findModel($id),
            'tags' => $tags,  // הטאג בלי הדולג הופך לטאג עם דולר בוויו
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() //אובייקט ריק מסוג ארטיקל
    {
        $model = new Article();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             // אם הגיעו נתונים מהבקשה (אפ ריקווסט) בשיטת פוסט וגם הצלחנו לשמור את הנתונים אז תכנס. אם לא נעמוד בחוקי הוליזציה לא יעבוד כי הפונקציה סייב (חלק מאקטיברקורד) לא עבדה!!
            return $this->redirect(['view', 'id' => $model->id]);
             //להעביר לוויו שהאיי די הוא המשתנה מודל
        }

        return $this->render('create', [ // אם לא נכנסנו לאיפ
            'model' => $model, //נשארים בטופס והערכים נשמרים
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) // חייב לעדכן מישהו ספציפי-מקבל איידי
    {
        $model = $this->findModel($id);

        //if (\Yii::$app->user->can('updateArticle',['article'=>$model])) { מוסרים את המאמר לחוק

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
   // }
   // throw new NotFoundHttpException('soory this isnt your article.'); //הודעת שגיאה
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete(); //פעולת מחיקה

        return $this->redirect(['index']); // חוזרים לרשימת האובייקטים
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) { // מחפש איי די שקיבלנו ביו-אר-אל ומכניס לנאל
            return $model; //מחזיר את האיי די שננמצא בתור מודל
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
