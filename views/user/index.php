<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
   

<?=Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => 
            [
                ['class' => 'yii\grid\SerialColumn'
                ],

                'id',
                'name',
                'email:email',
                'username',
                'auth_key',
                //'password',
                //'created_at',
                //'updated_at',
                //'crated_by',
                //'updated_by',
                [
                    'class' => 'yii\grid\ActionColumn',
                   // 'template' => '{view} {update} {delete} {myButton}',  // the default buttons + your custom button
                   // 'buttons' => [
                    //    'myButton' => function($model) {     // render your custom button
                     //       if (\Yii::$app->user->can('manageUsers', ['post' => $model])){
                     //       return Html::a('Create User', ['create'], ['class' => 'btn btn-success']); }
                      //  }
                   // ],
                    'visibleButtons' => [
                        'update' => function ($model) {
                            return \Yii::$app->user->can('manageUsers', ['post' => $model]);
                        },
                        'delete' => function ($model) {
                            return \Yii::$app->user->can('manageUsers', ['post' => $model]);
                        },
                        'create' => function ($model) {
                            return \Yii::$app->user->can('manageUsers', ['post' => $model]);
                        },
                    ]
                ],
    ]
    ]
    ); ?>
</div>
