<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Article;

/**
 * ArticleSearch represents the model behind the search form of `app\models\Article`.
 */
class ArticleSearch extends Article //יורש ויודע לפי זה על איזה טבלה הוא מדבר
{
    /***
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author_id', 'editor_id', 'category_id', 'created_by', 'updated_by'], 'integer'],
            [['title', 'descriptin', 'body', 'created_at', 'updated_at','tag'], 'safe'],
        ];
    }

    public $tag; //מגדיר תכונה שנקאת טאגז

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) //מקבלת פרמטר מהיו אר אל
    {
        $query = Article::find(); // שמים בקווארי את כל המאמרים הקיימים 

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([ 
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([ // לוקחים את הקווארי ומוסיפים לה תנאים
            'id' => $this->id, // מחפשים איי די שמופיע באיי די שביו אר אל- שבעצם נשלח מהיו אר אל, שמגיע מההקלדה
            'author_id' => $this->author_id,
            'editor_id' => $this->editor_id,
            'category_id' => $this->category_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]) // סינון לשדות של סטרינג
            ->andFilterWhere(['like', 'descriptin', $this->descriptin])
            ->andFilterWhere(['like', 'body', $this->body]);

            //תנאי תגים
        if(!empty($this->tag)) { // תנאי שמסנן לפי התגית 
        //צריך לבדוק האם התגית שביו אר אל מופיעה במאמר כלשהו
        $condition = Tag::find()->select('id')->where(['IN','name', $this->tag]);//יודע להפעיל על הטבלה טאג מהמחלקה טאג
        //יוצר רשימה של איי די של התגיות שביו אר אל
        $query->joinWith('tags'); // אם הולכים לארטיקל נראה גט טאגס, פונקציה שיוצרת קישור בין המחלקה ארטיקל לבין הטגים, לא צריך לכתוב גט טאגס כי זו פונקציה גטאר וזה יחשב כתכונה
        // השאילתה שרצה כבר
        $query->andWhere(['IN','tag_id', $condition]); // , נחפש את אלה שהטאג איי די שלהם נמצא בקונדישן-מוסיפים תנאי לקווארי שעושה את הסינון
    
         }

        return $dataProvider;
    }
}
