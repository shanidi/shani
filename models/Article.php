<?php

namespace app\models;
use yii\behaviors\BlameableBehavior; //עוזר לזה שמי שיצר משהו ישמר כך אוטומטית
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use dosamigos\taggable\Taggable;
use Yii;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $title
 * @property string $descriptin
 * @property string $body
 * @property int $author_id
 * @property int $editor_id
 * @property int $category_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article'; //חייב לקשר למחלקה את האקטיברקורד
    }
    public function behaviors() {
        return [
            BlameableBehavior::className(),
            [
                'class' => Taggable::className(),
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()  //חוקים הנוגעים לדף
    {
        return [
            [['body'], 'string'],
            [['author_id', 'editor_id', 'category_id'], 'integer'],// רק סעיפים שיגיעו מהטופס
           // [['created_at'], 'safe'], //סייפ- לא צריך יותר כי הסעיף לא מופיע יותר בטופס
            [['title', 'descriptin'], 'string', 'max' => 255],
            [['tagNames'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [//,תצוגה לתוויות
            'id' => 'ID',
            'title' => 'Title',
            'descriptin' => 'Descriptin',
            'body' => 'Body',
            'author_id' => 'Author ID',
            'editor_id' => 'Editor ID',
            'category_id' => 'Category',//הורדנו את הID 
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    public function getCategory()
    {
        return $this->hasOne(Category::className(),['id'=>'category_id']); //בגלל שלכל מאמר יש קטגוריה אחת
       //התכונה קטגורי איידי מחוברת לתכונה איידי במחלקה קטגורי- קשר אחד לאחד
    }
    
    public function getTags()
{
    return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('article_tag_assn', ['article_id' => 'id']);
}
}
