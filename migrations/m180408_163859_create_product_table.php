<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m180408_163859_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(), /*הדיס זה כי יורשים מהמחלקה ואנו רוצים לפנות לשיטה של המחלקה*/
            'title' => $this->string(),
            'body' => $this->text(),
            'category_id' => $this->integer(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product');
    }
}
